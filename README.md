# Tumblr Followers Check

## How to Deploy (to Heroku)

This repository does not include the built (`dist`) folder. So, in order to use `git` as deployment method some config is required.

1. Besides `heroku/nodejs` it's also required the custom buildpack in https://github.com/heroku/heroku-buildpack-static  

        heroku buildpacks:add heroku/nodejs --app <app_name>
        heroku buildpacks:add https://github.com/heroku/heroku-buildpack-static.git --app <app_name>

2. Create a `static.json` to setup the custom buildpack:

        {
            "root": "dist/",
            "clean_urls": true,
            "routes": {
                "/**": "index.html"
            }
        }

3. Force the installation of devDependencies as the project will be build on Heroku after deploy (See https://devcenter.heroku.com/articles/nodejs-support#devdependencies)

        heroku config:set NPM_CONFIG_PRODUCTION=false --app <app_name>

4. Add a script to `package.json` to ensure the project is build (and therefore `dist` is created)

        "scripts": {
            ...
            "heroku-postbuild": "webpack"
        }


