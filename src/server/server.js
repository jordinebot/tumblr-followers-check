import OAuth      from 'oauth';
import bodyParser from 'body-parser';
import cors       from 'cors';
import express    from 'express';
import path       from 'path';
import tumblr     from 'tumblr.js';
import session    from 'express-session';

const clientPath = path.resolve(__dirname, 'client');

const sessionSettings = {
    secret: 'ad637a29a486f34ab714565dd7d7c16f54efb945',
    resave: false,
    saveUninitialized: false,
    cookie: {
        secure: false,
    },
};

let oauthService;
let tumblrClient;
let tumblrConsumer;
let tumblrRequestToken;
let tumblrAccessToken;

let initTumblrClient = (consumerKey, consumerSecret, token, tokenSecret) => {
    tumblrClient = new tumblr.Client({
        credentials : {
            consumer_key:    consumerKey,
            consumer_secret: consumerSecret,
            token:           token,
            token_secret:    tokenSecret,
        },
        returnPromises: true,
    });
};

let invalidSession = () => {
    // TODO: Maybe redirect to '/'
    return res.status(403).send('Session not valid');
}

/* Create an Express App and use `assets` directory as static resources folder */
let app = express()
    .use(cors())
    .use(session(sessionSettings))
    .use(bodyParser.json())
    .use('/js', express.static(clientPath + '/js'))
    .use('/img', express.static(clientPath + '/img'));

/* This route gets called twice. The former is a POST request with Consumer
 * keys to retrieve the Request Token from Tumblr. The last one is a GET request
 * from Tumblr App callback param with `oauth_token` and `oauth_verifier` that allows
 * getting the final Access Token.
 */
app.post('/auth/oauth1', (req, res) => {

    let sess = req.session;

    if (!req.body.oauth_token) {
        oauthService = new OAuth.OAuth(
            'https://www.tumblr.com/oauth/request_token',
            'https://www.tumblr.com/oauth/access_token',
            req.body.clientId,
            req.body.clientSecret,
            '1.0A',
            null,
            'HMAC-SHA1'
        );

        tumblrConsumer = {
            key: req.body.clientId,
            secret: req.body.clientSecret,
        };

        oauthService.getOAuthRequestToken({
            oauth_callback: req.body.redirectUri
        }, (error, oauthToken, oauthTokenSecret, results) => {
            tumblrRequestToken = {
                token: oauthToken,
                secret: oauthTokenSecret,
            }

            if (error) {
                res.status(500).json(error);
            } else {
                res.json({
                    oauth_token: oauthToken,
                    oauth_token_secret: oauthTokenSecret,
                });
            }
        });

    } else {

        oauthService.getOAuthAccessToken(
            req.body.oauth_token,
            tumblrRequestToken.secret,
            req.body.oauth_verifier,
            (error, oauthAccessToken, oauthAccessTokenSecret, results) => {
                if (error) {
                    res.status(500).json(error);
                }

                tumblrAccessToken = {
                    token: oauthAccessToken,
                    secret: oauthAccessTokenSecret,
                };

                initTumblrClient(
                    tumblrConsumer.key,
                    tumblrConsumer.secret,
                    oauthAccessToken,
                    oauthAccessTokenSecret
                );

                sess.valid = true;

                res.json({
                    access_token: oauthAccessToken,
                    access_token_secret: oauthAccessTokenSecret,
                });

            }
        );
    }

});

app.get('/tumblr/:object', (req, res) => {
    if (!req.session.valid) invalidSession();

    if (typeof tumblrClient[req.params.object] === 'function') {
        tumblrClient[req.params.object](req.query)
            .then(data => {
                return res.json(data);
            })
            .catch(err => {
                return res.status(503).send(req.session);
            });
    } else {
        res.status(404).send('Object not found on Tumblr API');
    }
});

app.get('/tumblr/?', (req, res) => {
    if (!req.session.valid) invalidSession();

    res.send('Please be more specific');
});

app.get('*', (req, res) => {
    res.sendFile(clientPath + '/index.html');
}) ;

app.listen((process.env.PORT || 8080), () => {
    console.log('Up and running!');
});
