import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';

module.exports = [
    /* CLIENT */
    {
        entry: path.resolve(__dirname, 'src') + '/client/js/index.js',
        output: {
            path: path.resolve(__dirname, 'dist') + '/client/js',
            filename: 'main.js',
        },
        plugins: [
            new CleanWebpackPlugin('dist'),
            new HtmlWebpackPlugin({
                filename: '../index.html',
                template: path.resolve(__dirname, 'src') + '/client/index.html',
                //minify: {collapseWhitespace: true},
            }),
        ],
        module: {
            // Special compilation rules
            loaders: [
                {
                    test: /\.html$/,
                    loader: 'html-loader',
                }, {
                    // Ask webpack to check: If this file ends with .js,
                    // then apply some transforms
                    test: /\.js$/,
                    // Transform it with babel
                    loader: 'babel-loader',
                    // Don't transform node_modules folder
                    // (which don't need to be compiled)
                    exclude: /node_modules/,
                }, {
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    loader: 'file-loader',
                    options: {
                        name: '../img/[hash].[ext]',
                    },
                }, {
                    test: /\.vue$/,
                    loader: 'vue-loader',
                    options: {
                        loaders: {
                            scss: 'vue-style-loader!css-loader!sass-loader', // <style lang="scss">
                        }
                    },
                },
            ],
        },
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.esm.js',
                'styles': path.resolve(__dirname, 'src') + '/client/scss/',
                'img': path.resolve(__dirname, 'src') + '/client/img/'
            },
        },
        node: {
            fs: 'empty',
        },
    },
    /* SERVER */
    {
        name: 'server',
        target: 'node',
        node: {
            __dirname: false,
        },
        entry: path.resolve(__dirname, 'src') + '/server/server.js',
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: 'server.js',
        },
    }
]
