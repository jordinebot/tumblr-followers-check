import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import VueAuthenticate from 'vue-authenticate';

import Profile from './components/profile.vue';
import LoginForm from './components/login.vue';

const msPerMinute = 60 * 1000;
const msPerHour   = msPerMinute * 60;
const msPerDay    = msPerHour * 24;
const msPerMonth  = msPerDay * 30;
const msPerYear   = msPerDay * 365;

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VueAuthenticate, {
    providers: {
        oauth1: {
            redirectUri: 'http://tumblr.followers-check.com',
            authorizationEndpoint: 'https://www.tumblr.com/oauth/authorize',
            clientId: 'UC4NqwgPbl29R6PL7mIsyhLm6ks8DZcbWbWEExzMckZXCeZdsB',
            clientSecret: '8O8lveGD37ZMs1zX7a8FL4pPvSrLhBnDRWqpImh3yQPwfBfykM',
        }
    }
});

Vue.filter('timeago', timestamp => {

    let elapsed = +new Date() - timestamp * 1000;

    if (elapsed < msPerMinute) {
        return Math.round(elapsed / 1000) + ' seconds ago';
    } else if (elapsed < msPerHour) {
        return Math.round(elapsed / msPerMinute) + ' minutes ago';
    } else if (elapsed < msPerDay ) {
        return Math.round(elapsed / msPerHour ) + ' hours ago';
    } else if (elapsed < msPerMonth) {
        return Math.round(elapsed / msPerDay) + ' days ago';
    } else if (elapsed < msPerYear) {
        return Math.round(elapsed / msPerMonth) + ' months ago';
    } else {
        return Math.round(elapsed / msPerYear ) + ' years ago';
    }

});

const routes = [
    { path: '/', component: LoginForm, name: 'home' },
    { path: '/profile', component: Profile, name: 'profile' },
];

const router = new VueRouter({
    routes,
    mode: 'history',
});

new Vue({
    router,
    methods: {
        authenticate(provider) {
            this.$auth.authenticate(provider)
                .then(response => {
                    // Execute application logic after successful social authentication
                    this.$router.push('/profile');
                })
                .catch(error => {
                    console.error(error);
                });
        }
    }

}).$mount('#app');
